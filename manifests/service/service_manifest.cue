package service

import (
  k "kumori.systems/kumori:kumori"
  f ".../components/frontend:component"
  i "kumori.systems/builtins/inbound:service"
)

#Artifact: {
  ref: name:  "service"
  description: {

    config: {
      parameter: {
        allowedIPList?: i.#IPList
        deniedIPList?: i.#IPList
      }
      resource: {
        servercert: k.#Certificate
        serverdomain: k.#Domain
      }
    }

    role: {
      frontend: {
        artifact: f.#Artifact
        config: resilience: description.config.resilience
      }
      httpinbound: {
        artifact: i.#Artifact
        config: {
          parameter: {
            type: "https"
            if description.config.parameter.allowedIPList != _|_ {
              accesspolicies: allowediplist: description.config.parameter.allowedIPList
            }
            if description.config.parameter.deniedIPList != _|_ {
              accesspolicies: deniediplist: description.config.parameter.deniedIPList
            }
          }
          resource: {
            servercert: description.config.resource.servercert
            serverdomain: description.config.resource.serverdomain
          }
          resilience: description.config.resilience
        }
      }
    }

    srv: {} // No service channels: the inbound is included as part of the service

    connect: {
      // inbound => frontend
      cinbound: {
        as: "lb"
        from: httpinbound: "inbound"
        to: frontend: restapi: _
      }
    }
  }
}
