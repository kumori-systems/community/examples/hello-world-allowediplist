package deployment

import (
  s ".../service:service"
)

#Deployment: {
  name: "helloworlddep"
  artifact: s.#Artifact
  config: {
    //
    // Set here your allowedIPList/deniedIPList (with IPs or CIDRs)
    //
    parameter: allowedIPList: [ "192.168.20.152" ]
    //parameter: deniedIPList: [ "192.168.20.152" ]
    resource: {
      servercert: certificate: "cluster.core/wildcard-test-kumori-cloud"
      serverdomain: domain: "allowediplistdomain"
    }
    scale: detail: frontend: hsize: 1
    resilience: 0
  }
}
