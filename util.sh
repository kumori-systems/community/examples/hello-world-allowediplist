#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
DEPLOYNAME="allowediplistdep"
DOMAIN="allowediplistdomain"
SERVICEURL="allowediplist-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"
KUMORI_INBOUND_VERSION="1.3.0"

KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

#
# Before deploy the service, adjust deployment manifest configuration
#

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency --delete kumori.systems/builtins/inbound
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  ${KAM_CMD} mod dependency kumori.systems/builtins/inbound/@${KUMORI_INBOUND_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- --wait 5m
  ;;

'deploy-all')
  $0 create-domain
  $0 deploy-service
  ;;

# Before exexute this, the deployment manifest must be changed!
'update-service')
  ${KAM_CMD} service update -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Updating allowed/denied ip list service" \
    --wait 5m
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  ;;

'test')
  curl https://${SERVICEURL}/hello
  ;;

'test-I')
  curl -I https://${SERVICEURL}/hello
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN --wait 5m
  ;;

'undeploy-all')
  $0 undeploy-service
  $0 delete-domain
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
